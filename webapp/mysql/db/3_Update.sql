update isuumo.estate
set
    rent_popularity = case
        when rent < 50000 then popularity
        when rent between 50000 and 100000-1 then popularity + 10000000
        when rent between 100000 and 150000-1 then popularity + 20000000
        when rent >= 150000 then popularity + 30000000
    end,
    door_height_popularity = case
        when door_height < 80 then popularity
        when door_height between 80 and 110-1 then popularity + 10000000
        when door_height between 110 and 150-1 then popularity + 20000000
        when door_height >= 150 then popularity + 30000000
    end,
    door_width_popularity = case
        when door_width < 80 then popularity
        when door_width between 80 and 110-1 then popularity + 10000000
        when door_width between 110 and 150-1 then popularity + 20000000
        when door_width >= 150 then popularity + 30000000
    end,
    door_min = IF(door_width < door_height, door_width, door_height),
    door_max = IF(door_width > door_height, door_width, door_height)
;

update isuumo.chair
set
    price_popularity = case
        when price < 3000 then popularity
        when price between 3000 and 6000-1 then popularity + 10000000
        when price between 6000 and 9000-1 then popularity + 20000000
        when price between 9000 and 12000-1 then popularity + 30000000
        when price between 12000 and 15000-1 then popularity + 40000000
        when price >= 15000 then popularity + 40000000
    end,
    height_popularity = case
        when height < 80 then popularity
        when height between 80 and 110-1 then popularity + 10000000
        when height between 110 and 150-1 then popularity + 20000000
        when height >= 150 then popularity + 30000000
    end,
    width_popularity = case
        when width < 80 then popularity
        when width between 80 and 110-1 then popularity + 10000000
        when width between 110 and 150-1 then popularity + 20000000
        when width >= 150 then popularity + 30000000
    end,
    depth_popularity = case
        when depth < 80 then popularity
        when depth between 80 and 110-1 then popularity + 10000000
        when depth between 110 and 150-1 then popularity + 20000000
        when depth >= 150 then popularity + 30000000
    end,
    color_popularity = case
        when color = "黒" then popularity
        when color = "白" then popularity + 10000000
        when color = "赤" then popularity + 20000000
        when color = "青" then popularity + 30000000
        when color = "緑" then popularity + 40000000
        when color = "黄" then popularity + 50000000
        when color = "紫" then popularity + 60000000
        when color = "ピンク" then popularity + 70000000
        when color = "オレンジ" then popularity + 80000000
        when color = "水色" then popularity + 90000000
        when color = "ネイビー" then popularity + 100000000
        when color = "ベージュ" then popularity + 110000000
    end
;
