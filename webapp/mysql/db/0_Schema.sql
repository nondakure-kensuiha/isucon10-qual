DROP DATABASE IF EXISTS isuumo;
CREATE DATABASE isuumo;

DROP TABLE IF EXISTS isuumo.estate;
DROP TABLE IF EXISTS isuumo.chair;

CREATE TABLE isuumo.estate (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(4096) NOT NULL,
  `thumbnail` varchar(128) NOT NULL,
  `address` varchar(128) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `rent` int(11) NOT NULL,
  `door_height` int(11) NOT NULL,
  `door_width` int(11) NOT NULL,
  `features` varchar(64) NOT NULL,
  `popularity` int(11) NOT NULL,
  `rent_popularity` int(11) NOT NULL default 0,
  `door_height_popularity` int(11) NOT NULL default 0,
  `door_width_popularity` int(11) NOT NULL default 0,
  `door_min` int(11) NOT NULL default 0,
  `door_max` int(11) NOT NULL default 0,
  PRIMARY KEY (`id`),
  KEY `i_estate_rent` (rent),
  KEY `i_estate_latitude` (latitude),
  KEY `i_estate_rent_popularity` (rent_popularity desc),
  KEY `i_estate_door_height_popularity` (door_height_popularity desc),
  KEY `i_estate_door_width_popularity` (door_width_popularity desc)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE isuumo.chair (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(4096) NOT NULL,
  `thumbnail` varchar(128) NOT NULL,
  `price` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `depth` int(11) NOT NULL,
  `color` varchar(64) NOT NULL,
  `features` varchar(64) NOT NULL,
  `kind` varchar(64) NOT NULL,
  `popularity` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `price_popularity` int(11) NOT NULL default 0,
  `height_popularity` int(11) NOT NULL default 0,
  `width_popularity` int(11) NOT NULL default 0,
  `depth_popularity` int(11) NOT NULL default 0,
  `color_popularity` int(11) NOT NULL default 0,
  PRIMARY KEY (`id`),
  KEY `i_chair_price` (price),
  KEY `i_chair_price_popularity` (price_popularity desc),
  KEY `i_chair_height_popularity` (height_popularity desc),
  KEY `i_chair_width_popularity` (width_popularity desc),
  KEY `i_chair_depth_popularity` (depth_popularity desc),
  Key `i_chair_color_popularity` (color_popularity desc)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
