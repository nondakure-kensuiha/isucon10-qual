import csv
import json
import subprocess
from io import StringIO
from os import getenv, path

import flask
import mysql.connector
from humps import camelize
from sqlalchemy.pool import QueuePool
from werkzeug.exceptions import BadRequest, NotFound

LIMIT = 20
NAZOTTE_LIMIT = 50

chair_search_condition = json.load(open("../fixture/chair_condition.json", "r"))
estate_search_condition = json.load(open("../fixture/estate_condition.json", "r"))

app = flask.Flask(__name__)

mysql_connection_env = {
    "host": 's1',
    "port": getenv("MYSQL_PORT", 3306),
    "user": getenv("MYSQL_USER", "isucon"),
    "password": getenv("MYSQL_PASS", "isucon"),
    "database": getenv("MYSQL_DBNAME", "isuumo"),
}

cnxpool = QueuePool(
    lambda: mysql.connector.connect(**mysql_connection_env),
    pool_size=20,
)


def select_all(query, *args, dictionary=True):
    cnx = cnxpool.connect()
    try:
        cur = cnx.cursor(dictionary=dictionary)
        cur.execute(query, *args)
        return cur.fetchall()
    finally:
        cnx.close()


def select_row(*args, **kwargs):
    rows = select_all(*args, **kwargs)
    return rows[0] if len(rows) > 0 else None


@app.route("/initialize", methods=["POST"])
def post_initialize():
    sql_dir = "../mysql/db"
    sql_files = [
        "0_Schema.sql",
        "1_DummyEstateData.sql",
        "2_DummyChairData.sql",
        "3_Update.sql",
    ]

    for sql_file in sql_files:
        command = f"mysql -h {mysql_connection_env['host']} -u {mysql_connection_env['user']} -p{mysql_connection_env['password']} -P {mysql_connection_env['port']} {mysql_connection_env['database']} < {path.join(sql_dir, sql_file)}"
        subprocess.run(["bash", "-c", command])

    return {"language": "python"}


@app.route("/api/estate/low_priced", methods=["GET"])
def get_estate_low_priced():
    rows = select_all(f"SELECT {ESTATE_COLS} FROM estate ORDER BY rent ASC, id ASC LIMIT %s", (LIMIT,))
    return {"estates": camelize(rows)}


@app.route("/api/chair/low_priced", methods=["GET"])
def get_chair_low_priced():
    rows = select_all(f"SELECT {CHAIR_COLS} FROM chair ORDER BY price ASC, id ASC LIMIT %s", (LIMIT,))
    return {"chairs": camelize(rows)}


CHAIR_COLS = "id," \
             "name," \
             "description," \
             "thumbnail," \
             "price," \
             "height," \
             "width," \
             "depth," \
             "color," \
             "features," \
             "kind," \
             "popularity," \
             "stock"


@app.route("/api/chair/search", methods=["GET"])
def get_chair_search():
    args = flask.request.args

    conditions = []
    params = []
    order_target = 'popularity'
    if args.get("priceRangeId"):
        i = int(args.get("priceRangeId"))
        if i >= len(chair_search_condition["price"]["ranges"]):
            raise BadRequest("priceRangeID invalid")
        conditions.append('price_popularity between %s and %s')
        params.append(10 ** 7 * i)
        params.append(10 ** 7 * (i + 1) - 1)
        order_target = 'price_popularity'

    if args.get("heightRangeId"):
        i = int(args.get("heightRangeId"))
        if i >= len(chair_search_condition["height"]["ranges"]):
            raise BadRequest("heightRangeId invalid")
        conditions.append('height_popularity between %s and %s')
        params.append(10 ** 7 * i)
        params.append(10 ** 7 * (i + 1) - 1)
        order_target = 'height_popularity'

    if args.get("widthRangeId"):
        i = int(args.get("widthRangeId"))
        if i >= len(chair_search_condition["width"]["ranges"]):
            raise BadRequest("widthRangeId invalid")
        conditions.append('width_popularity between %s and %s')
        params.append(10 ** 7 * i)
        params.append(10 ** 7 * (i + 1) - 1)
        order_target = 'width_popularity'

    if args.get("depthRangeId"):
        i = int(args.get("depthRangeId"))
        if i >= len(chair_search_condition["depth"]["ranges"]):
            raise BadRequest("depthRangeId invalid")
        conditions.append('depth_popularity between %s and %s')
        params.append(10 ** 7 * i)
        params.append(10 ** 7 * (i + 1) - 1)
        order_target = 'depth_popularity'

    if args.get("kind"):
        conditions.append("kind = %s")
        params.append(args.get("kind"))

    if args.get("color"):
        color_index = color_to_index(args.get("color"))
        conditions.append('color_popularity between %s and %s')
        params.append(10 ** 7 * color_index)
        params.append(10 ** 7 * (color_index + 1) - 1)
        order_target = 'color_popularity'

    if args.get("features"):
        for feature_confition in args.get("features").split(","):
            conditions.append("features LIKE CONCAT('%', %s, '%')")
            params.append(feature_confition)

    if len(conditions) == 0:
        raise BadRequest("Search condition not found")

    try:
        page = int(args.get("page"))
    except (TypeError, ValueError):
        raise BadRequest("Invalid format page parameter")

    try:
        per_page = int(args.get("perPage"))
    except (TypeError, ValueError):
        raise BadRequest("Invalid format perPage parameter")

    search_condition = " AND ".join(conditions)

    query = f"SELECT COUNT(*) as count FROM chair WHERE {search_condition}"
    count = select_row(query, params)["count"]

    query = f"SELECT {CHAIR_COLS} FROM chair " \
            f"WHERE {search_condition} " \
            f"ORDER BY {order_target} DESC, id ASC " \
            f"LIMIT %s OFFSET %s"
    chairs = select_all(query, params + [per_page, per_page * page])

    return {"count": count, "chairs": camelize(chairs)}


@app.route("/api/chair/search/condition", methods=["GET"])
def get_chair_search_condition():
    return chair_search_condition


@app.route("/api/chair/<int:chair_id>", methods=["GET"])
def get_chair(chair_id):
    chair = select_row(f"SELECT {CHAIR_COLS} FROM chair WHERE id = %s", (chair_id,))
    if chair is None:
        raise NotFound()
    return camelize(chair)


@app.route("/api/chair/buy/<int:chair_id>", methods=["POST"])
def post_chair_buy(chair_id):
    cnx = cnxpool.connect()
    try:
        cnx.start_transaction()
        cur = cnx.cursor(dictionary=True)
        cur.execute("SELECT stock FROM chair WHERE id = %s FOR UPDATE", (chair_id,))
        chair = cur.fetchone()
        if chair is None:
            raise NotFound()
        if chair['stock'] > 1:
            cur.execute("UPDATE chair SET stock = stock - 1 WHERE id = %s", (chair_id,))
        else:
            cur.execute("DELETE FROM chair WHERE id = %s", (chair_id,))
        cnx.commit()
        return {"ok": True}
    except Exception as e:
        cnx.rollback()
        raise e
    finally:
        cnx.close()


ESTATE_COLS = "id," \
              "name," \
              "description," \
              "thumbnail," \
              "address," \
              "latitude," \
              "longitude," \
              "rent," \
              "door_height," \
              "door_width," \
              "features," \
              "popularity"


@app.route("/api/estate/search", methods=["GET"])
def get_estate_search():
    args = flask.request.args

    conditions = []
    params = []
    order_target = 'popularity'

    if args.get("doorHeightRangeId"):
        i = int(args.get("doorHeightRangeId"))
        if i >= len(estate_search_condition["doorHeight"]["ranges"]):
            raise BadRequest("doorHeightRangeId invalid")
        conditions.append('door_height_popularity between %s and %s')
        params.append(10 ** 7 * i)
        params.append(10 ** 7 * (i + 1) - 1)
        order_target = 'door_height_popularity'

    if args.get("doorWidthRangeId"):
        i = int(args.get("doorWidthRangeId"))
        if i >= len(estate_search_condition["doorWidth"]["ranges"]):
            raise BadRequest("doorWidthRangeId invalid")
        conditions.append('door_width_popularity between %s and %s')
        params.append(10 ** 7 * i)
        params.append(10 ** 7 * (i + 1) - 1)
        order_target = 'door_width_popularity'

    if args.get("rentRangeId"):
        i = int(args.get("rentRangeId"))
        if i >= len(estate_search_condition["rent"]["ranges"]):
            raise BadRequest("rentRangeId invalid")
        conditions.append('rent_popularity between %s and %s')
        params.append(10 ** 7 * i)
        params.append(10 ** 7 * (i + 1) - 1)
        order_target = 'rent_popularity'

    if args.get("features"):
        for feature_confition in args.get("features").split(","):
            conditions.append("features LIKE CONCAT('%', %s, '%')")
            params.append(feature_confition)

    if len(conditions) == 0:
        raise BadRequest("Search condition not found")

    try:
        page = int(args.get("page"))
    except (TypeError, ValueError):
        raise BadRequest("Invalid format page parameter")

    try:
        per_page = int(args.get("perPage"))
    except (TypeError, ValueError):
        raise BadRequest("Invalid format perPage parameter")

    search_condition = " AND ".join(conditions)

    query = f"SELECT COUNT(*) as count FROM estate WHERE {search_condition}"
    count = select_row(query, params)["count"]

    query = f"SELECT {ESTATE_COLS} FROM estate WHERE {search_condition} ORDER BY {order_target} DESC, id ASC LIMIT %s OFFSET %s"
    chairs = select_all(query, params + [per_page, per_page * page])

    return {"count": count, "estates": camelize(chairs)}


@app.route("/api/estate/search/condition", methods=["GET"])
def get_estate_search_condition():
    return estate_search_condition


@app.route("/api/estate/req_doc/<int:estate_id>", methods=["POST"])
def post_estate_req_doc(estate_id):
    estate = select_row("SELECT 1 FROM estate WHERE id = %s", (estate_id,))
    if estate is None:
        raise NotFound()
    return {"ok": True}


@app.route("/api/estate/nazotte", methods=["POST"])
def post_estate_nazotte():
    if "coordinates" not in flask.request.json:
        raise BadRequest()
    coordinates = flask.request.json["coordinates"]
    if len(coordinates) == 0:
        raise BadRequest()
    longitudes = [c["longitude"] for c in coordinates]
    latitudes = [c["latitude"] for c in coordinates]
    bounding_box = {
        "top_left_corner": {"longitude": min(longitudes), "latitude": min(latitudes)},
        "bottom_right_corner": {"longitude": max(longitudes), "latitude": max(latitudes)},
    }

    polygon_text = f"POLYGON(({','.join(['{} {}'.format(c['latitude'], c['longitude']) for c in coordinates])}))"

    cnx = cnxpool.connect()
    try:
        cur = cnx.cursor(dictionary=True)
        cur.execute(
            (
                f"SELECT {ESTATE_COLS} FROM estate"
                " WHERE (latitude between %s AND %s) AND (longitude between %s AND %s) AND "
                f"ST_Contains(ST_PolygonFromText('{polygon_text}'),"
                " ST_GeomFromText(CONCAT('POINT(', latitude, ' ', longitude, ')')))"
                " ORDER BY popularity DESC, id"
                " LIMIT %s"
            ),
            (
                bounding_box["top_left_corner"]["latitude"],
                bounding_box["bottom_right_corner"]["latitude"],
                bounding_box["top_left_corner"]["longitude"],
                bounding_box["bottom_right_corner"]["longitude"],
                NAZOTTE_LIMIT,
            ),
        )
        estates = cur.fetchall()
    finally:
        cnx.close()

    results = {"estates": [camelize(estate) for estate in estates]}
    results["count"] = len(results["estates"])
    return results


@app.route("/api/estate/<int:estate_id>", methods=["GET"])
def get_estate(estate_id):
    estate = select_row(f"SELECT {ESTATE_COLS} FROM estate WHERE id = %s", (estate_id,))
    if estate is None:
        raise NotFound()
    return camelize(estate)


@app.route("/api/recommended_estate/<int:chair_id>", methods=["GET"])
def get_recommended_estate(chair_id):
    chair = select_row("SELECT width, height, depth FROM chair WHERE id = %s", (chair_id,))
    if chair is None:
        raise BadRequest(f"Invalid format searchRecommendedEstateWithChair id : {chair_id}")
    chair_size_asc = sorted([chair["width"], chair["height"], chair["depth"]])
    query = (
        f"SELECT {ESTATE_COLS} FROM estate"
        " WHERE (%s <= door_min AND %s <= door_max)"
        " ORDER BY popularity DESC, id ASC"
        " LIMIT %s"
    )
    estates = select_all(query, (chair_size_asc[0], chair_size_asc[1], LIMIT))
    return {"estates": camelize(estates)}


@app.route("/api/chair", methods=["POST"])
def post_chair():
    if "chairs" not in flask.request.files:
        raise BadRequest()
    records = csv.reader(StringIO(flask.request.files["chairs"].read().decode()))
    cnx = cnxpool.connect()
    try:
        cnx.start_transaction()
        cur = cnx.cursor()
        params = []
        count = 0
        for record in records:
            params.extend(record)
            popularity = int(record[11])
            params.append(get_price_popularity(int(record[4]), popularity))
            params.append(get_height_popularity(int(record[5]), popularity))
            params.append(get_width_popularity(int(record[6]), popularity))
            params.append(get_depth_popularity(int(record[7]), popularity))
            params.append(get_color_popularity(record[8], popularity))
            count += 1
        query = "INSERT INTO chair(" \
                "id, " \
                "name, " \
                "description, " \
                "thumbnail, " \
                "price, " \
                "height, " \
                "width, " \
                "depth, " \
                "color, " \
                "features, " \
                "kind, " \
                "popularity, " \
                "stock, " \
                "price_popularity, " \
                "height_popularity, " \
                "width_popularity, " \
                "depth_popularity, " \
                "color_popularity" \
                ") VALUES %s" \
                % ",".join(["(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"] * count)
        cur.execute(query, params)
        cnx.commit()
        return {"ok": True}, 201
    except Exception as e:
        cnx.rollback()
        raise e
    finally:
        cnx.close()


def get_price_popularity(price, popularity):
    if price < 3000:
        correction = 0
    elif price < 6000:
        correction = 10 ** 7
    elif price < 9000:
        correction = 10 ** 7 * 2
    elif price < 12000:
        correction = 10 ** 7 * 3
    elif price < 15000:
        correction = 10 ** 7 * 4
    else:
        correction = 10 ** 7 * 5
    return correction + popularity


def get_height_popularity(height, popularity):
    if height < 80:
        correction = 0
    elif height < 110:
        correction = 10 ** 7
    elif height < 150:
        correction = 10 ** 7 * 2
    else:
        correction = 10 ** 7 * 3
    return correction + popularity


def get_width_popularity(width, popularity):
    if width < 80:
        correction = 0
    elif width < 110:
        correction = 10 ** 7
    elif width < 150:
        correction = 10 ** 7 * 2
    else:
        correction = 10 ** 7 * 3
    return correction + popularity


def get_depth_popularity(depth, popularity):
    if depth < 80:
        correction = 0
    elif depth < 110:
        correction = 10 ** 7
    elif depth < 150:
        correction = 10 ** 7 * 2
    else:
        correction = 10 ** 7 * 3
    return correction + popularity


def get_color_popularity(color, popularity):
    correction = 10 ** 7 * color_to_index(color)
    return correction + popularity


def color_to_index(color_str):
    try:
        return [
            "黒",
            "白",
            "赤",
            "青",
            "緑",
            "黄",
            "紫",
            "ピンク",
            "オレンジ",
            "水色",
            "ネイビー",
            "ベージュ"
        ].index(color_str)
    except ValueError:
        return -1


@app.route("/api/estate", methods=["POST"])
def post_estate():
    if "estates" not in flask.request.files:
        raise BadRequest()
    records = csv.reader(StringIO(flask.request.files["estates"].read().decode()))
    cnx = cnxpool.connect()
    try:
        cnx.start_transaction()
        cur = cnx.cursor()
        params = []
        count = 0
        for record in records:
            params.extend(record)
            popularity = int(record[11])
            params.append(get_rent_popularity(int(record[7]), popularity))
            door_height = int(record[8])
            door_width = int(record[9])
            params.append(get_door_height_popularity(door_height, popularity))
            params.append(get_door_width_popularity(door_width, popularity))
            params.append(min(door_width, door_height))
            params.append(max(door_width, door_height))
            count += 1
        query = "INSERT INTO estate(" \
                "id, " \
                "name, " \
                "description, " \
                "thumbnail, " \
                "address, " \
                "latitude, " \
                "longitude, " \
                "rent, " \
                "door_height, " \
                "door_width, " \
                "features, " \
                "popularity, " \
                "rent_popularity, " \
                "door_height_popularity, " \
                "door_width_popularity, " \
                "door_min, " \
                "door_max" \
                ") VALUES %s" \
                % ",".join(["(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"] * count)
        cur.execute(query, params)
        cnx.commit()
        return {"ok": True}, 201
    except Exception as e:
        cnx.rollback()
        raise e
    finally:
        cnx.close()


def get_rent_popularity(rent, popularity):
    if rent < 50000:
        correction = 0
    elif rent < 100000:
        correction = 10 ** 7
    elif rent < 150000:
        correction = 10 ** 7 * 2
    else:
        correction = 10 ** 7 * 3
    return correction + popularity


def get_door_height_popularity(door_height, popularity):
    if door_height < 80:
        correction = 0
    elif door_height < 110:
        correction = 10 ** 7
    elif door_height < 150:
        correction = 10 ** 7 * 2
    else:
        correction = 10 ** 7 * 3
    return correction + popularity


def get_door_width_popularity(door_width, popularity):
    if door_width < 80:
        correction = 0
    elif door_width < 110:
        correction = 10 ** 7
    elif door_width < 150:
        correction = 10 ** 7 * 2
    else:
        correction = 10 ** 7 * 3
    return correction + popularity


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=getenv("SERVER_PORT", 1323), debug=True, threaded=True)
