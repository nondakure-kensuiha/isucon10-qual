#!/bin/bash

export NEW_RELIC_CONFIG_FILE=/home/isucon/isuumo/webapp/python/newrelic.ini

cd /home/isucon/isuumo/webapp/python
/home/isucon/local/python/bin/uwsgi \
    --ini app.ini
